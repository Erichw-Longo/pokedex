import {Component, Inject, Input, OnInit} from '@angular/core';
import {Pokemon, typeChips} from '../Pokemon';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {PokemonService} from '../pokemon.service';
import {AuthService} from 'src/app/auth/auth.service';
import {AddPokemonComponent} from '../add-pokemon/add-pokemon.component';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {
  type1Color: string;
  type2Color: string;
  cry: any;

  @Input() pokemon: Pokemon;
  @Input() pokedex: Pokemon[];

  constructor(private pokemonService: PokemonService,
              private authService: AuthService,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.pokemon = this.data.pokemon;
    this.pokedex = this.data.pokedex;
    this.getAssets();
  }

  playCry() {
    this.cry.load();
    this.cry.play();
  }

  switchPokemon(id: number) {
    this.pokemon = this.pokedex.find(pkmn => pkmn.id === id);
    this.getAssets();
  }

  isAuthenticated(): boolean {
    return this.authService.isAuthenticated;
  }

  addPokemon() {
    const dialogRef = this.dialog.open(AddPokemonComponent, {
      data: {pokemon: this.pokemon}
    });

    dialogRef.afterClosed().subscribe();
  }

  getAssets() {
    this.type1Color = typeChips.find(type => type.type === (this.pokemon.types.type1).toLowerCase()).color;
    if (this.pokemon.types.type2) {
      this.type2Color = typeChips.find(type => type.type === (this.pokemon.types.type2).toLowerCase()).color;
    }
    this.cry = new Audio();
    this.pokemonService.getCry(this.pokemon.name).subscribe(res => {
      this.cry.src = `../../../assets/pokemon-cries/${res}`;
    });
  }
}
