import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Pokemon, Trainer} from './Pokemon';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {BASEURL} from '../constants';

const url = BASEURL + 'Pokedex';
const cryURL = BASEURL + 'cry';
const avatarURL = BASEURL + 'users/avatars';
const trainerURL = BASEURL + 'users/getTrainer';
const pokemonURL = BASEURL + 'users/addPokemon';

@Injectable({
  providedIn: 'root'
})

export class PokemonService {

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  getPokemonList(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(url);
  }

  addPokemonToCollection(pokemon: Pokemon) {
    return this.http.post(pokemonURL, pokemon, this.auth.tokenHeader).pipe(
      catchError(this.handleError('addPokemonToCollection', pokemon))
    );
  }

  updatePokemon(pokemon: Pokemon): Observable<any> {
    return this.http.post(BASEURL + '/users/updatePokemon', pokemon, this.auth.tokenHeader).pipe(
      catchError(this.handleError('updatePokemon', pokemon))
    );
  }

  deletePokemon(pokemon: Pokemon): Observable<any> {
    return this.http.post(BASEURL + `/users/deletePokemon`, pokemon, this.auth.tokenHeader).pipe(
      catchError(this.handleError('deletePokemon', pokemon))
    );
  }

  changeAvatar(avatar: string): Observable<any> {
    return this.http.post(avatarURL, {avatar: avatar}, this.auth.tokenHeader).pipe(
      catchError(this.handleError('changeAvatar'))
    );
  }

  getTrainerInfo(): Observable<any> {
    return this.http.get<Trainer>(trainerURL, this.auth.tokenHeader).pipe(
      catchError(this.handleError('getTrainer'))
    );
  }

  getCry(pokemon: string) {
    const params = new HttpParams().set('pokemon', pokemon);
    return this.http.get(cryURL, {params: params, responseType: 'text'});
  }

  getAvatars(): Observable<any> {
    return this.http.get(avatarURL).pipe(
      catchError(this.handleError('getAvatars'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption (Error handling service)
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
