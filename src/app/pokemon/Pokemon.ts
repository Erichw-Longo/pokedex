export interface Pokemon {
  id: number;
  name: string;
  types: {
    type1: string;
    type2?: string;
  };
  description: string;
  image: string;
  cry?: string;
  icon?: string;
  classification?: string;
  nickname?: string;
  areaCaught?: string;
}

export interface Trainer {
  trainerName: string;
  avatar: string;
  email: string;
  pokemon: Pokemon[];
}

export const typeChips = [
  { type: 'normal', color: '#a8a878' },
  { type: 'bug', color: '#a8b820' },
  { type: 'ground', color: '#705848' },
  { type: 'dark', color: '#a8a878' },
  { type: 'ice', color: '#98d8d8' },
  { type: 'dragon', color: '#7038f8' },
  { type: 'electric', color: '#f8d030' },
  { type: 'poison', color: '#a040a0' },
  { type: 'fighting', color: '#c03028' },
  { type: 'psychic', color: '#f85888' },
  { type: 'fire', color: '#f08030' },
  { type: 'rock', color: '#b8a038' },
  { type: 'flying', color: '#a890f0' },
  { type: 'steel', color: '#a8a878' },
  { type: 'ghost', color: '#705898' },
  { type: 'water', color: '#6890f0' },
  { type: 'grass', color: '#78c850' },
];
