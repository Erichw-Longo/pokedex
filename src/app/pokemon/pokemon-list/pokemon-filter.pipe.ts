import {Pipe, PipeTransform} from '@angular/core';
import {Pokemon} from '../Pokemon';

@Pipe({
  name: 'pokemonFilter'
})
export class PokemonFilterPipe implements PipeTransform {
  transform(pokemonList: Pokemon[], filterBy: string): Pokemon[] {
    if (filterBy === '') {
      return pokemonList;
    }
    return pokemonList.filter((lilPokeGuy) => lilPokeGuy.name.toLowerCase().includes(filterBy.toLowerCase()));
  }
}
