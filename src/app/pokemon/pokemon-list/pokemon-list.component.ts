import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../Pokemon';
import { PokemonService } from '../pokemon.service';
import { MatDialog } from '@angular/material';
import { PokemonDetailComponent } from '../pokemon-detail/pokemon-detail.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  selectedPokemon: Pokemon;
  pokemonList: Pokemon[];
  nameFilter = new FormControl('');

  constructor(private pokemonService: PokemonService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getPokemon();
  }

  openDetails(pkmn: Pokemon) {
    this.selectedPokemon = pkmn;
    this.pokemonDetail();
  }

  getPokemon() {
    this.pokemonService.getPokemonList().subscribe(response => {
      this.pokemonList = response;
      this.pokemonList = this.pokemonList.sort((a, b) => (a.id > b.id) ? 1 : (a.id < b.id) ? -1 : 0);
    });
  }

  pokemonDetail() {
    const dialogRef = this.dialog.open(PokemonDetailComponent, {
      data: {pokemon: this.selectedPokemon, pokedex: this.pokemonList}
    });

    dialogRef.afterClosed().subscribe();
  }

}
