import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { PokemonService } from '../pokemon.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Pokemon } from '../Pokemon';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-add-pokemon',
  templateUrl: './add-pokemon.component.html',
  styleUrls: ['./add-pokemon.component.css']
})
export class AddPokemonComponent implements OnInit {

  @Input() pokemon: Pokemon;
  addForm: FormGroup;
  nickname: FormControl;
  areaCaught: FormControl;

  constructor(private pokemonService: PokemonService,
    public authService: AuthService,
    public notificationService: NotificationService,
    public dialogRef: MatDialogRef<AddPokemonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.addForm = new FormGroup({
      nickname: new FormControl('', [Validators.required]),
      areaCaught: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
    localStorage.removeItem('error');
    this.pokemon = this.data.pokemon;
  }

  isAuthenticated(): boolean {
    return this.authService.isAuthenticated;
  }

  onSubmit() {
    const pokemon = this.pokemon;
    pokemon.areaCaught = this.addForm.get('areaCaught').value;
    pokemon.nickname = this.addForm.get('nickname').value;
    this.pokemonService.addPokemonToCollection(pokemon).subscribe((res) => {
      this.notificationService.showMessage(`${pokemon.name} added to collection.`, `Success`, { duration: 5000 });
      this.dialogRef.close();
    });
  }
}
