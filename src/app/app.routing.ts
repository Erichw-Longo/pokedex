import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './auth/register/register.component';
import {PokemonListComponent} from './pokemon/pokemon-list/pokemon-list.component';
import {PokemonDetailComponent} from './pokemon/pokemon-detail/pokemon-detail.component';
import {LoginComponent} from './auth/login/login.component';
import {AddPokemonComponent} from './pokemon/add-pokemon/add-pokemon.component';
import {FriendsComponent} from './friends/main/friends.component';
import {TrainerComponent} from './trainer/trainer.component';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'pokemon-list',
    component: PokemonListComponent
  },
  {
    path: 'pokemon-detail',
    component: PokemonDetailComponent
  },
  {
    path: 'add-pokemon',
    component: AddPokemonComponent
  },
  {
    path: 'friends',
    component: FriendsComponent
  },
  {
    path: 'trainer',
    component: TrainerComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];
export const RouterLinks = RouterModule.forRoot(appRoutes);
