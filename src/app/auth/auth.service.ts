import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { RegisterUser } from './register/register';
import { Router } from '@angular/router';
import { LoginUser } from './login/login';

const URL = 'http://localhost:3000/api/v1/users';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  NAME_KEY = 'name';
  TOKEN_KEY = 'token';
  ERROR_KEY = 'error';
  constructor(private http: HttpClient, private router: Router) { }

  // Getters
  get name() {
    return localStorage.getItem(this.NAME_KEY);
  }

  get isAuthenticated() {
    return !!localStorage.getItem(this.TOKEN_KEY);
  }

  get error() {
    return localStorage.getItem(this.ERROR_KEY);
  }

  get tokenHeader() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY) });
    return { headers };
  }

  registerUser(user: RegisterUser): Observable<any> {
    localStorage.removeItem(this.ERROR_KEY);
    return this.http.post<RegisterUser>(URL + '/register', user).pipe(
      catchError(this.handleError('registerUser', user))
    );
  }

  loginUser(user: LoginUser): Observable<any> {
    localStorage.removeItem(this.ERROR_KEY);
    return this.http.post<LoginUser>(URL + '/login', user).pipe(
      catchError(this.handleError('loginUser', user))
    );
  }

  checkTrainerName(trainerName: string): Observable<string> {
    localStorage.removeItem(this.ERROR_KEY);
    const params = new HttpParams().set('trainername', trainerName);
    return this.http.get<string>(`${URL}/trainername/`, { params: params })
      .pipe(catchError(this.handleError('checkTrainerName', trainerName))
      );
  }

  checkEmail(email: string): Observable<string> {
    localStorage.removeItem(this.ERROR_KEY);
    const params = new HttpParams().set('email', email);
    return this.http.get<string>(`${URL}/email/`, { params: params })
      .pipe(catchError(this.handleError('checkEmail', email))
      );
  }

  authenticate(res) {
    localStorage.removeItem(this.ERROR_KEY);
    localStorage.setItem(this.TOKEN_KEY, res.token);
    localStorage.setItem(this.NAME_KEY, res.trainerName);
    this.router.navigate(['home']);
    location.reload();
    // TODO: Return to where the user intended to go
  }

  logout() {
    localStorage.removeItem(this.ERROR_KEY);
    localStorage.removeItem(this.NAME_KEY);
    localStorage.removeItem(this.TOKEN_KEY);
    if (this.router.url !== 'home') {
      this.router.navigate(['home']);
    }
    location.reload();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption (Error handling service)
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
