import { Component, OnInit } from '@angular/core';
import { RegisterUser } from './register';
import { FormControl, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { ValidationService } from '../../shared/services/validation.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  passwordFocus = false;
  registerForm: FormGroup;
  trainerName: FormControl;
  password: FormControl;
  passwordRepeat: FormControl;
  email: FormControl;

  constructor(public validationService: ValidationService, public authService: AuthService) {
    this.registerForm = new FormGroup({
      trainerName: new FormControl('', {
        validators: Validators.compose([Validators.required,
        this.checkTrainerName.bind(this)]), updateOn: 'blur'
      }),
      password: new FormControl('', Validators.compose([Validators.required])),
      passwordRepeat: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', {
        validators: Validators.compose([Validators.required,
        this.validationService.ValidateEmail, this.checkEmail.bind(this)]), updateOn: 'blur'
      }),
    }, this.validationService.MatchPassword);
  }

  ngOnInit() {
    localStorage.removeItem('error');
  }

  toggleFocus(state: boolean) {
    this.passwordFocus = state;
  }

  checkTrainerName(control) {
    if (this.registerForm && control.value) {
      this.authService.checkTrainerName(control.value).subscribe(res => {
        if (parseInt(res, 10) === 1) {
          control.setErrors({ nameTaken: true });
          return { nameTaken: true };
        }
        return null;
      });
    }
  }

  checkEmail(control: AbstractControl) {
    if (this.registerForm && control.value) {
      this.authService.checkEmail(control.value).subscribe(res => {
        if (parseInt(res, 10) === 1) {
          control.setErrors({ emailTaken: true });
          return { emailTaken: true };
        }
        return null;
      });
    }
  }

  clearForm() {
    this.registerForm.reset();
    this.registerForm.updateValueAndValidity();
  }

  onSubmit() {
    const user: RegisterUser = this.registerForm.value;
    this.authService.registerUser(user).subscribe((res) => {
      const authResponse = res;

      if (!authResponse.token) {
        localStorage.setItem('error', 'Invalid trainer name or password');
        return;
      }
      this.authService.authenticate(res);
    });
  }
}
