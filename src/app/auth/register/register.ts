export interface RegisterUser {
  trainerName: string;
  password: string;
  email: string;
}
