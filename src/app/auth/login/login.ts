export interface LoginUser {
  trainerName: string;
  password: string;
}
