import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { LoginUser } from './login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  trainerName: FormControl;
  password: FormControl;

  constructor(public authService: AuthService) {
    this.loginForm = new FormGroup({
      trainerName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
    localStorage.removeItem('error');
  }

  onSubmit() {
    const user: LoginUser = this.loginForm.value;
    this.authService.loginUser(user).subscribe((res) => {
      const authResponse = res;

      if (!authResponse.token) {
        localStorage.setItem('error', 'Invalid trainer name or password');
        return;
      }
      this.authService.authenticate(res);
    });
  }

  clearForm() {
    localStorage.removeItem('error');
    this.loginForm.reset();
    this.loginForm.updateValueAndValidity();
  }

}
