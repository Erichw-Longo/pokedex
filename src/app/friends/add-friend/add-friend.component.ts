import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FriendsService} from '../friends.service';
import {NotificationService} from '../../shared/services/notification.service';
import {Friend} from '../Friend';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.css']
})
export class AddFriendComponent implements OnInit {
  @Output() back = new EventEmitter<boolean>();

  addForm: FormGroup;

  name: FormControl;
  system: FormControl;
  description: FormControl;
  cry: FormControl;
  classification: FormControl;
  nickname: FormControl;
  areaCaught: FormControl;

  newFriendo: Friend;

  constructor(private friendService: FriendsService, public notificationService: NotificationService) {
    this.addForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      system: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      cry: new FormControl('', []),
      classification: new FormControl('', []),
      nickname: new FormControl('', []),
      areaCaught: new FormControl('', []),
    });
  }

  ngOnInit() {
    this.newFriendo = {name: '', system: ''};
  }

  onAddPress() {
    if (this.addForm.valid) {
      this.newFriendo.name = this.addForm.get('name').value;
      this.newFriendo.system = this.addForm.get('system').value;
      this.newFriendo.description = this.addForm.get('description').value;
      this.newFriendo.cry = this.addForm.get('cry').value;
      this.newFriendo.classification = this.addForm.get('classification').value;
      this.newFriendo.nickname = this.addForm.get('nickname').value;
      this.newFriendo.areaCaught = this.addForm.get('areaCaught').value;
      this.friendService.addFriend(this.newFriendo).subscribe(() => {
        this.notificationService.showMessage(`${this.newFriendo.name} added to friends.`, `Success`, {duration: 5000});
        this.backed(false);
      });
    }
  }

  onCancelPress() {
    this.backed(true);
  }

  backed(canceled: boolean) {
    this.back.emit(canceled);
  }

}
