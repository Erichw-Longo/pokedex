export interface Friend {
  name: string;
  system: string;
  description?: string;
  cry?: string;
  classification?: string;
  nickname?: string;
  areaCaught?: string; // I consider friends to be more or less caught
}
