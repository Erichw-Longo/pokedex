import {Component, Input, OnInit} from '@angular/core';
import {Friend} from '../Friend';
import {NotificationService} from '../../shared/services/notification.service';

@Component({
  selector: 'app-friends-list',
  templateUrl: './friends-list.component.html',
  styleUrls: ['./friends-list.component.css']
})
export class FriendsListComponent {

  @Input() friends: Array<Friend>;

  constructor(public notificationService: NotificationService) {}

  friendClick(friendo: Friend) {
    this.notificationService.showMessage(`${friendo.name} goes: ${friendo.cry}!`, ``, {duration: 2000});
  }

  getNickname(friendo: Friend) {
    if (friendo.nickname) {
      return `(${friendo.nickname})`;
    }
    return '';
  }

  getAreaTxt(friendo: Friend) {
    if (friendo.areaCaught) {
      return ` , you know, the Digimon Trainer you found in ${friendo.areaCaught}!`;
    }
    return '';
  }
}
