import {Component, OnInit} from '@angular/core';
import {FriendsService} from '../friends.service';
import {Friend} from '../Friend';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {
  friendList = Array<Friend>();
  addFriend = false;

  constructor(private friendService: FriendsService) {
  }

  ngOnInit() {
    this.getFriends();
  }

  getFriends() {
    this.friendService.getFriendList().subscribe(response => {
      this.friendList = response;
    });
  }

  onAddClick() {
    this.addFriend = true;
  }

  onBack($event) {
    this.addFriend = false;
    this.getFriends();
  }

}
