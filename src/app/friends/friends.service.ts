import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {AuthService} from '../auth/auth.service';
import {BASEURL} from '../constants';
import {Friend} from './Friend';
import {catchError} from 'rxjs/operators';

const addURL = BASEURL + 'users/addFriend';
const getFriendsURL = BASEURL + 'users/friends';

@Injectable({
  providedIn: 'root'
})
export class FriendsService {

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  getFriendList(): Observable<Friend[]> {
    return this.http.get<Friend[]>(getFriendsURL, this.auth.tokenHeader);
  }

  addFriend(friend: Friend) {
    return this.http.post(addURL, friend, this.auth.tokenHeader);
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption (Error handling service)
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
