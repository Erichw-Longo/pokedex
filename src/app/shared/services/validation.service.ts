import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  ValidateEmail(control: AbstractControl) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    return !EMAIL_REGEXP.test(control.value) ? { invalidEmail: true } : null;
  }

  MatchPassword(group: FormGroup) {
    const pass = group.get('password').value;
    const confirmPass = group.get('passwordRepeat').value;

    return pass === confirmPass ? null : { notSame: true };
  }

}
