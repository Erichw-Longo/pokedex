import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(public snackBar: MatSnackBar) { }

  showMessage(msg: string, lbl: string, config: any) {
    this.snackBar.open(msg, lbl, config);
  }
}
