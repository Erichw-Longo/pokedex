import { Component, OnInit } from '@angular/core';
import { Trainer, typeChips, Pokemon } from '../pokemon/Pokemon';
import { PokemonService } from '../pokemon/pokemon.service';
import { NotificationService } from '../shared/services/notification.service';
import { AddPokemonComponent } from '../pokemon/add-pokemon/add-pokemon.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  constructor(private pokemonService: PokemonService,
    public notificationService: NotificationService,
    public dialog: MatDialog) { }

  trainer: Trainer;
  pokemon: Pokemon[];
  colours: any[];
  avatars: any[];
  selectedAvatar: number;
  editToggle = false;
  newToggle = false;
  pokemonEditToggles: boolean[];
  pokemonEditToggle: boolean;

  ngOnInit() {
    this.populateAvatars();
    this.getTrainerInfo();
    this.populatePokemon();
    this.pokemonEditToggles = [];
  }

  getTrainerInfo() {
    this.colours = [];
    this.pokemonService.getTrainerInfo().subscribe(trainerRes => {
      this.trainer = trainerRes;
      trainerRes.pokemon.forEach(pkmn => {
        const colour1 = typeChips.find(type => type.type === (pkmn.types.type1).toLowerCase()).color;
        if (pkmn.types.type2) {
          const colour2 = typeChips.find(type => type.type === (pkmn.types.type2).toLowerCase()).color;
          this.colours.push({ colour1: colour1, colour2: colour2 });
        } else {
          this.colours.push({ colour1: colour1, colour2: null });
        }
      });
      if (trainerRes.avatar) {
        this.trainer.avatar = trainerRes.avatar;
        this.selectedAvatar = this.avatars.indexOf(this.trainer.avatar);
      } else {
        this.selectedAvatar = 0;
      }
    });
  }

  populateAvatars() {
    this.pokemonService.getAvatars().subscribe(avatars => {
      this.avatars = avatars;
    });
  }

  populatePokemon() {
    this.pokemonService.getPokemonList().subscribe(pokemon => {
      this.pokemon = pokemon;
      this.pokemon = this.pokemon.sort((a, b) => (a.id > b.id) ? 1 : (a.id < b.id) ? -1 : 0);
    });
  }

  switchPicture(op: boolean) {
    // Change avatar
    if (op === false) {
      this.selectedAvatar--;
      if (this.selectedAvatar < 0) {
        this.selectedAvatar = this.avatars.length - 1;
      }
    } else {
      this.selectedAvatar++;
      if (this.selectedAvatar > this.avatars.length - 1) {
        this.selectedAvatar = 0;
      }
    }
  }

  updateTrainer() {
    this.pokemonService.changeAvatar(this.avatars[this.selectedAvatar]).subscribe(response => {
      if (!!response) {
        this.notificationService.showMessage(`Avatar updated.`, `Success`, { duration: 5000 });
      } else {
        this.notificationService.showMessage(`Error updating Avatar.`, `Fail`, { duration: 5000 });
      }
      this.editToggle = false;
    });
  }

  toggleEdit() {
    this.editToggle = !this.editToggle;
  }

  toggleNew() {
    this.newToggle = !this.newToggle;
  }

  addPokemon(selection) {

    const dialogRef = this.dialog.open(AddPokemonComponent, {
      data: { pokemon: selection.value }
    });

    dialogRef.afterClosed().subscribe(() => {
      location.reload();
    });
  }

  editPokemon(id: number) {
    if (!this.pokemonEditToggle || this.pokemonEditToggles[id]) {
      if (!this.pokemonEditToggles[id]) {
        this.pokemonEditToggles[id] = true;
        this.pokemonEditToggle = true;
      } else {
        this.pokemonEditToggles[id] = false;
        this.pokemonEditToggle = false;
      }
    }
  }

  nickname(contents, pkmn) {
    pkmn.nickname = contents.srcElement.value;
  }

  areaCaught(contents, pkmn) {
    pkmn.areaCaught = contents.srcElement.value;
  }

  updatePokemon(pkmn: Pokemon) {
    this.pokemonService.updatePokemon(pkmn).subscribe(response => {
      this.notificationService.showMessage(`${pkmn.nickname} updated.`, `Success`, { duration: 5000 });
      this.trainer = response;
      this.pokemonEditToggles[pkmn.id] = false;
      this.pokemonEditToggle = false;
    });
  }

  deletePokemon(pkmn: Pokemon) {
    this.pokemonService.deletePokemon(pkmn).subscribe(response => {
      this.notificationService.showMessage(`${pkmn.nickname} deleted.  Goodnight sweet prince.`, `Success`, { duration: 5000 });
      this.trainer = response;
    });
  }

}
