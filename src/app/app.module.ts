import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterLinks} from './app.routing';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './auth/register/register.component';
import {PokemonListComponent} from './pokemon/pokemon-list/pokemon-list.component';
import {PokemonDetailComponent} from './pokemon/pokemon-detail/pokemon-detail.component';
import {PokemonFilterPipe} from './pokemon/pokemon-list/pokemon-filter.pipe';
import {LoginComponent} from './auth/login/login.component';
import {AddPokemonComponent} from './pokemon/add-pokemon/add-pokemon.component';
import {FriendsComponent} from './friends/main/friends.component';
import {FriendsListComponent} from './friends/friends-list/friends-list.component';
import {AddFriendComponent} from './friends/add-friend/add-friend.component';
import {TrainerComponent} from './trainer/trainer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    PokemonListComponent,
    PokemonDetailComponent,
    PokemonFilterPipe,
    LoginComponent,
    AddPokemonComponent,
    FriendsComponent,
    FriendsListComponent,
    AddFriendComponent,
    TrainerComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    RouterLinks,
    RouterModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
