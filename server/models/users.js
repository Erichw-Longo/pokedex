const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

// schema maps to a collection
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  trainerName: {
    type: 'String',
    required: true,
    trim: true,
    unique: true
  },
  email: {
    type: 'String',
    required: true,
    trim: true,
    unique: true
  },
  password: {
    type: 'String',
    required: true,
    trim: true
  },
  avatar: {
    type: 'String',
    trim: true
  },
  pokemon: [{
    id: {
      type: 'number',
    },
    name: {
      type: 'String',
    },
    classification: {
      type: 'String',
    },
    types: {
      type1: {
        type: 'String',
      },
      type2: {
        type: 'String',
      }
    },
    description: {
      type: 'String',
    },
    nickname: {
      type: 'String',
    },
    areaCaught: {
      type: 'String',
    },
  }],
  friends: [{
    name: 'String',
    system: 'String',
    description: 'String',
    image: {data: Buffer, contentType: 'String'},
    cry: 'String',
    classification: 'String',
    nickname: 'String',
    areaCaught: 'String'
  }]
});

module.exports = mongoose.model('User', UserSchema);


