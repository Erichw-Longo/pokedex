const mongoose = require('mongoose');
// schema maps to a collection
const Schema = mongoose.Schema;

const FriendSchema = new Schema({
  name: String,
  system: String,
  description: String,
  img: { data: Buffer, contentType: String },
  cry: String,
  classification: String,
  nickname: String,
  areaCaught: String,
});

export {FriendSchema};


