import mongoose from 'mongoose';

// schema maps to a collection
const Schema = mongoose.Schema;

export const PokemonSchema = new Schema({
  id: {
    type: 'number',
    required: true,
    trim: true,
    unique: true
  },
  name: {
    type: 'String',
    required: true,
    trim: true,
    unique: true
  },
  classification: {
    type: 'String',
  },
  types: {
    type1: {
      type: 'String',
      required: true,
      trim: true,
    },
    type2: {
      type: 'String',
    }
  },
  description: {
    type: 'String',
    required: true,
    trim: true,
    unique: true
  },
  nickname: {
    type: 'String',
  },
  areaCaught: {
    type: 'String',
  },
  moveset: [{
    move: {
      type: 'String',
    },
    description: {
      type: 'String',
    },
    type: {
      type: 'String',
    },
    level: {
      type: 'Number',
    },
    pp: {
      type: 'String',
    },
    power: {
      type: 'String',
    },
    accuracy: {
      type: 'String',
    },
  }],
  required: false,
});
