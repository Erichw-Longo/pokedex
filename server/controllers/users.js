import mongoose from 'mongoose';
import * as jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';
import {UserSchema} from '../models/users';
import bcrypt from 'bcrypt';

const User = mongoose.model("User", UserSchema);
const saltRounds = 12;
const connUri = `${process.env.DB_HOST_LOCAL}${process.env.DB_USER}`;

export const register = (req, res) => {
  mongoose.connect(connUri, {useNewUrlParser: true}, (err) => {

    if (!err) {
      const {trainerName, email, password} = req.body;
      const user = new User({trainerName, email, password});

      bcrypt.hash(user.password, saltRounds, function (err, hash) {
        user.password = hash;
        user.save((err, user) => {
          if (!err) {
            const response = {
              id: user.id,
              trainerName: user.trainerName,
            };
            sendToken(response, res);
          } else {
            res.status(500);
            res.send(err);
          }
        });
      })
    }
  });
};

export const addFriend = async (req, res) => {
  const id = checkAuthenticated(req, res);
  if (id !== -1) {
    User.findOne({_id: id}, (err, user) => {
      if (err) {
        res.status(500);
        res.send(err);
      } else {
        user.friends.push(req.body);
        user.save((err, user) => {
          if (!err) {
            const response = {
              id: user.id,
              trainerName: user.trainerName,
              friends: user.friends
            };
            res.send(response);

          }
        });
      }
    });
  }
};

export const getFriends = async (req, res) => {
  const id = checkAuthenticated(req, res);
  if (id !== -1) {
    User.findOne({_id: id}, (err, user) => {
      if (err) {
        res.status(500);
        res.send(err);
      }
      res.status(200);
      user ? res.send(user.friends) : res.send([]);
    })
  }
};

export const login = async (req, res) => {
  await User.findOne({trainerName: req.body.trainerName}, 'id trainerName password', (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    if (!user) {
      res.status(401);
      res.send('Username or password invalid.');
    } else {

      bcrypt.compare(req.body.password, user.password, function (err, result) {
        if (result === true) {
          const response = {
            id: user.id,
            trainerName: user.trainerName,
          };
          sendToken(response, res);
        } else {
          res.status(401);
          res.send('Password invalid.');
        }
      });
    }
  });
};

export const checkTrainerName = async (req, res) => {
  await User.findOne({trainerName: req.query.trainername}, (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    res.status(200);
    user ? res.send('1') : res.send('0');
  })
};

export const checkEmail = async (req, res) => {
  await User.findOne({
    email: req.query.email
  }, (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    res.status(200);
    user ? res.send('1') : res.send('0');
  })
};

export const addToCollection = async (req, res) => {
  const id = checkAuthenticated(req, res);
  if (id === -1) {
    res.status(401).send({ message: 'Unauthorized request. Authentication header invalid' });
  }
  await User.findOne({ _id: id }, (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    user.pokemon.push(req.body);
    user.save((err, user) => {
      if (!err) {

        const response = {
          id: user.id,
          trainerName: user.trainerName,
          pokemon: user.pokemon
        };
        res.send(response);
      } else {
        res.status(500);
        res.send(err);
      }
    });
  })
};

export const updatePokemon = async (req, res) => {
  const id = checkAuthenticated(req, res);
  if (id === -1) {
    res.status(401).send({ message: 'Unauthorized request. Authentication header invalid' });
  }
  await User.findOne({ _id: id }, (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    const index = user.pokemon.findIndex(pkmn => pkmn._id == req.body._id);
    user.pokemon[index] = req.body;

    user.save((err, user) => {
      if (!err) {

        const response = {
          id: user.id,
          trainerName: user.trainerName,
          pokemon: user.pokemon
        }
        res.send(response);
      } else {
        res.status(500);
        res.send(err);
      }
    });
  })
}

export const deletePokemon = async (req, res) => {
  const id = checkAuthenticated(req, res);
  if (id === -1) {
    res.status(401).send({ message: 'Unauthorized request. Authentication header invalid' });
  }
  await User.findOne({ _id: id }, (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }

    const index = user.pokemon.indexOf(req.body);
    user.pokemon.splice(index);

    user.save((err, user) => {
      if (!err) {

        const response = {
          id: user.id,
          trainerName: user.trainerName,
          pokemon: user.pokemon
        }
        res.send(response);
      } else {
        res.status(500);
        res.send(err);
      }
    });
  })
}

export const getAvatars = (req, res) => {
  fs.readdir(path.join(__dirname, "../../src/assets/trainer"), (err, files) => {
    if (err) {
      res.send(err)
    }
    res.send(files);
  });
};

export const updateAvatar = async (req, res) => {
  const id = checkAuthenticated(req, res);

  if (id === -1) {
    res.status(401).send({ message: 'Unauthorized request. Authentication header invalid' });
  }
  await User.findOne({ _id: id }, (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    user.avatar = req.body.avatar;
    user.save((err, user) => {
      if (!err) {

        const response = {
          id: user.id,
          trainerName: user.trainerName,
          pokemon: user.pokemon,
          avatar: user.avatar
        };
        res.send(response);
      } else {
        res.status(500);
        res.send(err);
      }
    });
  })
};

export const getTrainer = async (req, res) => {
  const id = checkAuthenticated(req, res);
  if (id === -1) {
    res.status(401).send({ message: 'Unauthorized request. Authentication header invalid' });
  }
  await User.findOne({ _id: id }, 'trainerName email pokemon avatar', (err, user) => {
    if (err) {
      res.status(500);
      res.send(err);
    }
    res.status(200);
    res.send(user);
  })
};

const sendToken = (user, res) => {
  const token = jwt.sign(user.id, '123');
  res.json({
    trainerName: user.trainerName,
    token
  });
};

const checkAuthenticated = (req, res) => {
  if (!req.header('authorization')) {
    res.status(401).send({
      message: 'Unauthorized requested. Missing authentication header'
    });
    return -1;
  }

  const token = req.header('authorization').split(' ')[1];
  const payload = jwt.decode(token, '123');

  if (!payload)
    return -1;

  req.user = payload;
  return req.user;
};
