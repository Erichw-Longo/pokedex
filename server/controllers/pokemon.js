import mongoose from 'mongoose';
import {MoveSchema, PokemonSchema} from '../models/pokemon';

const fs = require('fs');
const path = require('path');
const Pokemon = mongoose.model("Pokedex", PokemonSchema, "Pokedex");

export const getPokemonList = (req, res) => {
  Pokemon.find({}, (err, pokemon) => {
    if (err) {
      res.send(err)
    }
    res.json(pokemon);
  })
};

export const getPokemonCry = (req, res) => {
  const pokemon = req.query.pokemon;
  let cry;
  fs.readdir(path.join(__dirname, "../../src/assets/pokemon-cries"), (err, files) => {
    if (err) {
      res.send(err)
    }
    for (let i = 0; i < files.length; i++) {
      if (files[i].includes(pokemon)) {
        cry = files[i];
        break;
      }
    }
    res.send(cry);
  });
};
