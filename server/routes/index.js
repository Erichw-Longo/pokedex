import users from './users';
import pokemon from './pokemon';

const routes = (router) => {
  users(router);
  pokemon(router);
  return router;
};

export default routes;
