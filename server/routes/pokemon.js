import { getPokemonList, getPokemonCry, getAvatars } from '../controllers/pokemon';
const pokemonRoutes = router => {
  router.route('/Pokedex').get(getPokemonList);
  router.route('/cry').get(getPokemonCry);
};

export default pokemonRoutes;
