import {
  addToCollection,
  checkEmail,
  checkTrainerName,
  getAvatars,
  getTrainer,
  login,
  register,
  updateAvatar,
  getFriends,
  addFriend,
  updatePokemon,
  deletePokemon
} from '../controllers/users';

const userRoutes = router => {
  router.route(`/users/friends`).get(getFriends);
  router.route(`/users/addFriend`).post(addFriend);
  router.route('/users/register').post(register);
  router.route('/users/login').post(login);
  router.route('/users/trainername').get(checkTrainerName);
  router.route('/users/email').get(checkEmail);
  router.route('/users/addPokemon').post(addToCollection);
  router.route('/users/getTrainer').get(getTrainer);
  router.route('/users/avatars').post(updateAvatar);
  router.route('/users/avatars').get(getAvatars);
  router.route('/users/avatars').get(getAvatars);
  router.route('/users/updatePokemon').post(updatePokemon);
  router.route('/users/deletePokemon').post(deletePokemon);

};

export default userRoutes;
