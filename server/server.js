import express from 'express';
import http from 'http';
import morgan from 'morgan';
import cors from 'cors';
import {} from 'dotenv/config';
import bodyParser from 'body-parser';
import errorHandler from 'errorhandler';
import mongoose from 'mongoose';
import routes from './routes/index.js';

const app = express();
const router = express.Router();

/**
 * Mongoose: Object modelling tool for Mongo, allows for use of schemas with mongo.
 */
mongoose.Promise = global.Promise;
// Disabling useFindAndModify as mongoose seems to use this by default and this is depracated in mongo https://github.com/Automattic/mongoose/issues/5616
mongoose.set('useFindAndModify', false);
mongoose.connect(`${process.env.DB_HOST_LOCAL}${process.env.DB_USER}`, {useNewUrlParser: true});

/**
 * Body parser set up
 */
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.set('port', process.env.PORT || 3000);

/**
 * Morgan: Gives us information in the console for each http operation performed on the api
 */
app.use(morgan('dev'));

/**
 * CORS: Cross origin resource sharing so that the server can be accessed from different URLs
 */
app.use(cors());

if (app.get('env') === 'development') {
  app.use(errorHandler());
}

app.use('/api/v1', routes(router));

const server = http.createServer(app);

server.listen(app.get('port'), function () {
  console.log(`Server listening on port ${app.get('port')}`);
  console.log(`Server base directory = ${__dirname}`);
})
